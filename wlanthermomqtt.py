#!/usr/bin/env python
'''
Created on 26.02.2017

@author: christophcastor
'''
from ConfigParser import ConfigParser
import paho.mqtt.client as mqtt
import mqttconfig
import time
import csv


import math

import os




def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    client.subscribe(mqttconfig.MAINTOPIC + "/"+ mqttconfig.ID+"/#")

def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))
    
def on_change_max_temp_0(client, userdata, msg):
    changeMaxTemp(0, msg.payload)
    print msg.payload
    

def on_change_max_temp_1(client, userdata, msg):
    changeMaxTemp(1, msg.payload)

def on_change_max_temp_2(client, userdata, msg):
    changeMaxTemp(2, msg.payload)
    
def on_change_max_temp_3(client, userdata, msg):
    changeMaxTemp(3, msg.payload)

def on_change_max_temp_4(client, userdata, msg):
    changeMaxTemp(4, msg.payload)

def on_change_max_temp_5(client, userdata, msg):
    changeMaxTemp(5, msg.payload)

def on_change_max_temp_6(client, userdata, msg):
    changeMaxTemp(6, msg.payload)
    
def on_change_max_temp_7(client, userdata, msg):
    changeMaxTemp(7, msg.payload)


    

def changeMaxTemp(index, value):
    value = int(round(float(value)))
    if(value != int(mqttconfig.currentMaxTemps[index])):
        
        
        mqttconfig.currentMaxTemps[index] = value
        config = ConfigParser() 
        config.read(mqttconfig.CONFPATH + mqttconfig.CONFFILE)
        config.set('temp_max', 'temp_max'+str(index), value)
        saveConfig(config)
    
def saveConfig(conf):
    with open(mqttconfig.CONFPATH + mqttconfig.CONFFILE+"_pytemp", 'w') as configfile:
    #with open(mqttconfig.CONFPATH + mqttconfig.CONFFILE, 'w') as configfile:
        conf.write(configfile)
    os.rename(mqttconfig.CONFPATH + mqttconfig.CONFFILE+"_pytemp", mqttconfig.CONFPATH + mqttconfig.CONFFILE)
        
def loadCurrentConfig():
    config = ConfigParser() 
    config.read(mqttconfig.CONFPATH + mqttconfig.CONFFILE)
    i=0
    while i < 8:
        mqttconfig.currentMaxTemps[i] = config.get("temp_max", "temp_max"+str(i))
        i=i+1
def checkMaxTemps(client, config):
    i=0
    while i < 8:
        if int(mqttconfig.currentMaxTemps[i]) != int(config.get("temp_max", "temp_max"+str(i))):
            client.publish(mqttconfig.MAINTOPIC+ "/" +mqttconfig.ID+ "/temp_max"+str(i), config.get("temp_max", "temp_max"+str(i)))
            mqttconfig.currentMaxTemps[i] = config.get("temp_max", "temp_max"+str(i))
        i=i+1
def getActTemps(client):
    csvfile = open(mqttconfig.CURRENTTEPPATH)
    tempReader = csv.reader(csvfile, delimiter=';')
    
    for row in tempReader:
        i=0
        while i < 8:
            
            val = row[i+1]
            if(val != "" and val != "er"):
                val = math.ceil(float(val)*100)/100
            else:
                val = 0.0
            if(mqttconfig.currentTemps[i] != val):
                client.publish(mqttconfig.MAINTOPIC+ "/" +mqttconfig.ID+ "/temp"+str(i), val)
                mqttconfig.currentTemps[i] = val  
            i=i+1
          
        break
  
        
config = ConfigParser()
#loadCurrentConfig()
connected = False
client = mqtt.Client()



client.message_callback_add(mqttconfig.MAINTOPIC+ "/" +mqttconfig.ID+ "/temp_max0", on_change_max_temp_0)
client.message_callback_add(mqttconfig.MAINTOPIC+ "/" +mqttconfig.ID+ "/temp_max1", on_change_max_temp_1)
client.message_callback_add(mqttconfig.MAINTOPIC+ "/" +mqttconfig.ID+ "/temp_max2", on_change_max_temp_2)
client.message_callback_add(mqttconfig.MAINTOPIC+ "/" +mqttconfig.ID+ "/temp_max3", on_change_max_temp_3)
client.message_callback_add(mqttconfig.MAINTOPIC+ "/" +mqttconfig.ID+ "/temp_max4", on_change_max_temp_4)
client.message_callback_add(mqttconfig.MAINTOPIC+ "/" +mqttconfig.ID+ "/temp_max5", on_change_max_temp_5)
client.message_callback_add(mqttconfig.MAINTOPIC+ "/" +mqttconfig.ID+ "/temp_max6", on_change_max_temp_6)
client.message_callback_add(mqttconfig.MAINTOPIC+ "/" +mqttconfig.ID+ "/temp_max7", on_change_max_temp_7)
client.on_message = on_message
client.on_connect = on_connect
client._client_id = "Wlanthermo"+str(mqttconfig.ID)
while connected == False:
    try:
        if mqttconfig.MQTTSERVER_USER:
            client.username_pw_set(mqttconfig.MQTTSERVER_USER, mqttconfig.MQTTSERVER_PW)
        client.connect(mqttconfig.MQTTSERVER, 1883, 30)
        client.loop_start()
        connected = True
    except:
        print "Could not connect to Server"
        time.sleep(10)


while True:
    config.read(mqttconfig.CONFPATH + mqttconfig.CONFFILE)
    checkMaxTemps(client, config)
    getActTemps(client)
    time.sleep(1)
    