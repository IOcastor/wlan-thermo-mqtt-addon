# README #

Simples Python script, um Die gemessenen Werte des [Wlan Thermos](https://github.com/WLANThermo/WLANThermo_v2/) per MQTT an eine Automation zu übertragen.

### Abhängigkeiten ###
pah-mqtt (sudo pip install paho-mqtt)
Ein mqtt Server z.B. [Mosquitto](https://mosquitto.org)

### Installation ###
Abhängigkeiten installieren und einrichten

```
#!/bin/bash
sudo pip install paho-mqtt
sudo apt-get install git
git clone https://IOcastor@bitbucket.org/IOcastor/wlan-thermo-mqtt-addon.git
cd wlan-thermo-mqtt-addon
sudo mkdir /usr/local/bin/wlanthermoaddon
sudo cp -r * /usr/local/bin/wlanthermoaddon/
sudo cp /usr/local/bin/wlanthermoaddon/initscript/wlanthermomqtt /etc/init.d/
sudo chmod +x /usr/local/bin/wlanthermoaddon/wlanthermomqtt.py
sudo chmod 755 /etc/init.d/wlanthermomqtt
sudo systemctl daemon-reload
sudo service wlanthermomqtt start
cd /etc/init.d
sudo update-rc.d wlanthermomqtt defaults 

```

### Konfiguration ###

Im Ordner /usr/local/bin/wlanthermoaddon/ befindet sich eine mqttconfig.py
Diese Anpassen und service neu starten(sudo service wlanthermomqtt restart).

```
CONFPATH = "/var/www/conf/"
CONFFILE = "WLANThermo.conf"
CURRENTTEPPATH = "/var/www/tmp/display/current.temp"

MQTTSERVER = "<MQTT Server ip>"
# remove comment, if authentication is not needed
# MQTTSERVER_USER = None
# comment out, if authentication is not needed
MQTTSERVER_USER = "admin"
MQTTSERVER_PW = "password"
MAINTOPIC = "wlanthermo"

ID = "<ID>"  des Wlan Thermos falls mehrere genutzt werden
```

### Smarthome Integration in Openhab2 ###
Es wurde in der Opelhab mqtt.cfg ein MQTT Broker namens mosquitto konfiguriert.
[Openhab2 Doku](http://docs.openhab.org/addons/bindings/mqtt1/readme.html)

```
#!/bin/bash
Group wlanthermo
Number thermo1_max_0	"Max Temp 1"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp_max0:state:default], >[mosquitto:wlanthermo/1/temp_max0:command:*:DEFAULT]"}
Number thermo1_max_1	"Max Temp 2"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp_max1:state:default], >[mosquitto:wlanthermo/1/temp_max1:command:*:DEFAULT]"}
Number thermo1_max_2	"Max Temp 3"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp_max2:state:default], >[mosquitto:wlanthermo/1/temp_max2:command:*:DEFAULT]"}
Number thermo1_max_3	"Max Temp 4"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp_max3:state:default], >[mosquitto:wlanthermo/1/temp_max3:command:*:DEFAULT]"}
Number thermo1_max_4	"Max Temp 5"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp_max4:state:default], >[mosquitto:wlanthermo/1/temp_max4:command:*:DEFAULT]"}
Number thermo1_max_5	"Max Temp 6"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp_max5:state:default], >[mosquitto:wlanthermo/1/temp_max5:command:*:DEFAULT]"}
Number thermo1_max_6	"Max Temp 7"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp_max6:state:default], >[mosquitto:wlanthermo/1/temp_max6:command:*:DEFAULT]"}
Number thermo1_max_7	"Max Temp 8"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp_max7:state:default], >[mosquitto:wlanthermo/1/temp_max7:command:*:DEFAULT]"}

Number thermo1_current_0 "Temperatur 1 [%.1f °C]"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp0:state:default]"}
Number thermo1_current_1 "Temperatur 2 [%.1f °C]"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp1:state:default]"}
Number thermo1_current_2 "Temperatur 3 [%.1f °C]"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp2:state:default]"}
Number thermo1_current_3 "Temperatur 4 [%.1f °C]"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp3:state:default]"}
Number thermo1_current_4 "Temperatur 5 [%.1f °C]"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp4:state:default]"}
Number thermo1_current_5 "Temperatur 6 [%.1f °C]"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp5:state:default]"}
Number thermo1_current_6 "Temperatur 7 [%.1f °C]"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp6:state:default]"}
Number thermo1_current_7 "Temperatur 8 [%.1f °C]"		(wlanthermo)	{ mqtt="<[mosquitto:wlanthermo/1/temp7:state:default]"}

```


### Hilfe ###

* Bei fragen gern per Mail: io@castor.io