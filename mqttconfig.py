'''
Created on 26.02.2017

@author: christophcastor
'''
CONFPATH = "/var/www/conf/"
CONFFILE = "WLANThermo.conf"
CURRENTTEPPATH = "/var/www/tmp/display/current.temp"

MQTTSERVER = "192.168.41.6"
# remove comment, if authentication is not needed
# MQTTSERVER_USER = None
# comment out, if authentication is not needed
MQTTSERVER_USER = "admin"
MQTTSERVER_PW = "password"
MAINTOPIC = "wlanthermo"
ID = "1"

#CURRENTTEPPATH = "current.temp"

currentMaxTemps = [0,0,0,0,0,0,0,0]
currentTemps = [0,0,0,0,0,0,0,0]